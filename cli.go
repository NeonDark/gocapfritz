package main

import (
	"errors"
	"flag"
	"fmt"
	"os"
)

// default interfaces
func getExtCapInterfaces() string {
	var str string
	str += "extcap {version=1.0}{help=https://gitlab.com/NeonDark/gocapfritz}\n"
	str += "interface {value=fritzbox}{display=Fritz!Box router capture}\n"

	return str
}

// extcap dlts
func getExtCapDLTs() string {
	return "dlt {number=1}{name=USER1}{display=Fritz!Box}"
}

// extcap argument fields
func getExtCapArgs() (arguments string) {
	arguments += "arg {number=0}{call=-h}{display=Host}{tooltip=FRITZ!Box host (URL or IP)}{type=string}{required=true}\n"
	arguments += "arg {number=1}{call=-p}{display=Password}{tooltip=FRITZ!Box password}{type=password}{required=true}\n"
	arguments += "arg {number=2}{call=-l}{display=Packet Length}{tooltip=FRITZ!Box capture packet length in bytes}{type=UNSIGNED}{default=1600}{required=true}\n"
	arguments += "arg {number=3}{call=-i}{display=Capture Interface}{type=selector}{reload=true}{placeholder=Load interfaces}{required=true}\n"
	return
}

// auto discover interfaces for extcap arguments
func getExtCapArgsLive(host string, pw string) (arguments string, err error) {
	sid, err := getSessionID(host, pw)

	if err != nil {
		return
	}

	interfaces, err := getAvailableInterfaces(sid, host)
	if err != nil {
		return
	}

	arguments += getExtCapArgs()

	for _, iface := range interfaces {
		arguments += "value {arg=3}{value=" + iface.value + "}{display=" + iface.title + "}\n"
	}

	return
}

// prints information to stdout if corresponding variable is true
// returns true if printing was successful
func printExtCap(printExtCapInterfaces bool, printExtCapDLTs bool,
	printExtCapConfig bool) bool {

	if printExtCapInterfaces {
		fmt.Println(getExtCapInterfaces())
		return true
	}

	if printExtCapDLTs {
		fmt.Println(getExtCapDLTs())
		return true
	}

	if printExtCapConfig {
		fmt.Println(getExtCapArgs())
		return true
	}

	return false
}

// load available capture interfaces from router using host and password
// extCapReloadOption must be equal to "-i"
// returns true successful
func doExtCapReload(extCapReloadOption string, printExtCapConfig bool, host string, pw string) bool {
	if !printExtCapConfig || extCapReloadOption != "-i" || len(host) == 0 || len(pw) == 0 {
		return false
	}

	strOut, err := getExtCapArgsLive(host, pw)

	if err != nil {
		// print if error getting live data
		fmt.Println(getExtCapArgs())
		return true
	}
	fmt.Println(strOut)

	return true
}

// adds "http://" in front and "/" in back if necessary
func optimizeHost(host string) string {
	if len(host) == 0 {
		return host
	}
	// add slash to host if needed
	if host[len(host)-1:] != "/" {
		host += "/"
	}

	// add "http://" at front if not found
	strHTTP := "http://"
	strHTTPS := "https://"
	httpLen := len(strHTTP)
	httpsLen := len(strHTTPS)

	// if len smaller than http strings, add http string
	if len(host) < httpsLen && len(host) < httpLen {
		host = strHTTP + host
	} else {
		// if http strings not found in front, add http string
		if host[:httpLen] != strHTTP && host[:httpsLen] != strHTTPS {
			host = strHTTP + host
		}
	}

	return host
}

func flagUsage() {
	var examples string

	examples += "./gocapfritz --extcap-interfaces\n"
	examples += "./gocapfritz --extcap-dlts\n"
	examples += "./gocapfritz --extcap-config\n"
	examples += "./gocapfritz --capture -p <password> -h <host> --fifo <path_to_file> -i <interface>\n"

	_, err := fmt.Fprintf(os.Stderr, "Usage of %s:\n", os.Args[0])
	if err != nil {
		panic(err)
	}
	_, err = fmt.Fprint(os.Stderr, examples)
	if err != nil {
		panic(err)
	}
	_, err = fmt.Fprintf(os.Stderr, "\n")
	if err != nil {
		panic(err)
	}
	_, err = fmt.Fprintf(os.Stderr, "Examples:\n")
	if err != nil {
		panic(err)
	}
	flag.PrintDefaults()
}

// get CLI-arguments. Check bool if all values are ok
func getArgs() (string, string, bool, string, string, uint, error) {
	// set flag usage output
	flag.Usage = flagUsage

	// handle CLI-arguments
	printExtCapInterfaces := flag.Bool("extcap-interfaces", false, "Prints interfaces available to extcap")
	printExtCapDLTs := flag.Bool("extcap-dlts", false, "Prints extcap DLTs")
	printExtCapConfig := flag.Bool("extcap-config", false, "Prints extcap config")
	extCapReloadOption := flag.String("extcap-reload-option", "", "Reload option for provided string")
	isCapture := flag.Bool("capture", false, "Set extcap capture active")
	// the version is not actually used, only the extcap requirements must be satisfied
	_ = flag.String("extcap-version", "", "Set extcap version")
	extCapInterface := flag.String("extcap-interface", "", "Set extcap interface")
	filePath := flag.String("fifo", "", "Path to output pcap file")
	help := flag.Bool("help", false, "Show help")
	password := flag.String("p", "", "FRITZ!Box password")
	host := flag.String("h", "", "FRITZ!Box host (URL or IP)")
	iface := flag.String("i", "", "FRITZ!Box capture interface")
	pLength := flag.Uint("l", 1600, "FRITZ!Box capture packet length in bytes")
	flag.Parse()

	if *help {
		flag.Usage()
		return *password, *host, *isCapture,
			*filePath, *extCapInterface, *pLength, errors.New("showed help")
	}

	*host = optimizeHost(*host)

	if doExtCapReload(*extCapReloadOption, *printExtCapConfig, *host, *password) {
		return *password, *host, *isCapture, *filePath, *extCapInterface, *pLength, errors.New("extCap")
	}

	// print extcap information and return
	if printExtCap(*printExtCapInterfaces, *printExtCapDLTs, *printExtCapConfig) {
		return *password, *host, *isCapture, *filePath, *extCapInterface, *pLength, errors.New("extCap")
	}

	// host, password and iface must not be empty
	if *host == "" || *password == "" || *iface == "" {
		flag.Usage()
		return *password, *host, *isCapture, *filePath, *extCapInterface,
			*pLength, errors.New("no host, password or interface provided")
	}

	return *password, *host, *isCapture, *filePath, *iface, *pLength, nil
}
