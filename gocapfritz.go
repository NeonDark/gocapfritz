package main

import (
	"fmt"
	"io"
	"os"
)

func main() {
	// get CLI-Args
	pw, host, isCapture, filePath, iface, pLength, err := getArgs()
	if err != nil || !isCapture {
		return
	}

	sid, err := getSessionID(host, pw)
	if err != nil {
		fmt.Println("Error getting session:", err)
		return
	}

	body, err := captureData(host, sid, iface, pLength)
	if err != nil {
		fmt.Println("Error capturing data:", err)
		return
	}

	// create file for streaming
	f, err := os.OpenFile(filePath, os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		fmt.Println("Error opening FIFO file", err)
		return
	}
	defer func(f *os.File) {
		err := f.Close()
		if err != nil {
			fmt.Println(err)
		}
	}(f)

	// stream data to file
	_, err = io.Copy(f, body)
	if err != nil {
		fmt.Println(err)
	}
}
