package main

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/xml"
	"errors"
	"golang.org/x/crypto/pbkdf2"
	"io"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"
)

const loginPath = "/login_sid.lua?version=2"

// do a GET request to the specified URL and return the body
func doRequest(url string, timeout time.Duration) (io.ReadCloser, error) {
	c := &http.Client{
		Timeout: timeout,
	}

	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	httpData, err := c.Do(request)
	if err != nil {
		return nil, err
	}

	if httpData.StatusCode != http.StatusOK {
		err = errors.New("request failed")
		return nil, err
	}

	return httpData.Body, nil
}

// do a POST request to the specified URL and return the body
func doPOST(u string, timeout time.Duration, parameters url.Values) (io.ReadCloser, error) {
	c := &http.Client{
		Timeout: timeout,
	}

	resp, err := c.PostForm(u, parameters)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		err = errors.New("request failed")
		return nil, err
	}

	return resp.Body, nil
}

type response struct {
	SID       string `xml:"SID"`
	Challenge string `xml:"Challenge"`
	Users     []struct {
		User string `xml:"User"`
	} `xml:"Users"`
}

// returns the auth challenge and the username
func getCredentials(host string) (challenge, username string, err error) {
	body, err := doRequest(host+loginPath, 15*time.Second)
	if err != nil {
		return
	}

	requestBody, err := io.ReadAll(body)
	if err != nil {
		return
	}

	var r response
	err = xml.Unmarshal(requestBody, &r)
	if err != nil {
		return
	}

	if len(r.Users) == 0 {
		err = errors.New("no user generated")
		return
	}

	challenge = r.Challenge
	username = r.Users[0].User

	return
}

func getResponse(host, pw string) (response, username string, err error) {
	challenge, username, err := getCredentials(host)
	if err != nil {
		return
	}

	parts := strings.Split(challenge, "$")
	if len(parts) != 5 || parts[0] != "2" {
		err = errors.New("unsupported challenge version")
		return
	}

	strIter1 := parts[1]
	hexSalt1 := parts[2]
	strIter2 := parts[3]
	hexSalt2 := parts[4]

	iter1, err := strconv.Atoi(strIter1)
	if err != nil {
		return
	}

	salt1, err := hex.DecodeString(hexSalt1)
	if err != nil {
		return
	}

	iter2, err := strconv.Atoi(strIter2)
	if err != nil {
		return
	}

	salt2, err := hex.DecodeString(hexSalt2)
	if err != nil {
		return
	}

	pass1 := pbkdf2.Key([]byte(pw), salt1, iter1, 32, sha256.New)
	pass2 := pbkdf2.Key(pass1, salt2, iter2, 32, sha256.New)

	response = hexSalt2 + "$" + hex.EncodeToString(pass2)
	return
}

func getSID(host, challengeResponse, username string) (sid string, err error) {
	body, err := doPOST(host+loginPath, 15*time.Second, url.Values{
		"response": {challengeResponse},
		"username": {username},
	})
	if err != nil {
		return
	}

	requestBody, err := io.ReadAll(body)
	if err != nil {
		return
	}

	var r response
	err = xml.Unmarshal(requestBody, &r)
	if err != nil {
		return
	}

	sid = r.SID

	return
}

func getSessionID(host string, pw string) (sid string, err error) {
	resp, username, err := getResponse(host, pw)
	if err != nil {
		return
	}

	sid, err = getSID(host, resp, username)
	if err != nil {
		return
	}

	if sid == "0000000000000000" {
		err = errors.New("SID is null after response")
		return
	}

	return
}

// get stream
func captureData(host string, sessionInfo string, iface string, packetLength uint) (body io.ReadCloser, err error) {
	body, err = doRequest(host+"cgi-bin/capture_notimeout?sid="+sessionInfo+"&ifaceorminor="+
		iface+"&capture=Start&snaplen="+strconv.FormatUint(uint64(packetLength), 10), 0)
	return
}
