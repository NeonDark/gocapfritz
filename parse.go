package main

import (
	"errors"
	"io"
	"net/http"
	"net/url"
	"strings"

	"golang.org/x/net/html"
)

type fritzInterface struct {
	title string // display title
	value string // interface identifier
}

// find all "data"-nodes (e.g. <a>) which are children of parent
func findNode(parent *html.Node, data string) []*html.Node {
	var nodes []*html.Node
	var crawler func(*html.Node)
	crawler = func(node *html.Node) {
		if node.Type == html.ElementNode && node.Data == data {
			nodes = append(nodes, node)
		}
		for child := node.FirstChild; child != nil; child = child.NextSibling {
			crawler(child)
		}
	}
	crawler(parent)

	return nodes
}

// get all available interfaces by discovering them directly from the fritz box
func getAvailableInterfaces(sid string, host string) (interfaces []fritzInterface, err error) {

	body, err := getDataHTML(sid, host)

	if err != nil {
		return
	}

	doc, err := html.Parse(body)

	if err != nil {
		return
	}

	tables := findNode(doc, "table")

	if tables == nil {
		err = errors.New("no tables found")
		return
	}

	for _, e := range tables {
		trs := findNode(e, "tr")
		if trs == nil {
			err = errors.New("no table row found")
			return
		}

		for _, tr := range trs {
			ths := findNode(tr, "th")

			if ths == nil {
				continue
			} else if len(ths) > 1 {
				err = errors.New("too many table headers found")
				return
			}

			buttons := findNode(tr, "button")

			if buttons == nil {
				err = errors.New("no button found")
				return
			}

			var val string
			for _, b := range buttons {
				foundButton := false

				for _, a := range b.Attr {

					if a.Key == "id" && strings.Contains(a.Val, "Start") {
						foundButton = true
					} else if a.Key == "value" {
						val = a.Val
					}
				}

				if foundButton {
					break
				}
			}

			// not start button found
			if val == "" {
				continue
			}

			interfaces = append(interfaces, fritzInterface{value: val, title: ths[0].FirstChild.Data})

		}
	}

	return
}

// returns reader for html body
func getDataHTML(sid string, host string) (body io.ReadCloser, err error) {

	formData := url.Values{
		"xhr":         {"1"},
		"sid":         {sid},
		"lang":        {"de"},
		"no_sidrenew": {""},
		"page":        {"cap"},
	}

	resp, err := http.PostForm(host+"data.lua", formData)

	if err != nil {
		return
	}

	if resp.StatusCode != http.StatusOK {
		err = errors.New("request failed")
		return
	}

	body = resp.Body

	return
}
