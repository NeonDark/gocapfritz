
# GoCapFritz

An extcap plugin for Wireshark providing access to the capture interfaces of a Fritz!Box Router. Written in Go without dependencies.

## Build

First install Go, this may vary based on your OS. Find help in the [Golang documentation](https://golang.org/doc/install).

Then

`go build .`

will produce `gocapfritz`.

## Commandline Usage

```shell
./gocapfritz --extcap-interfaces

./gocapfritz --extcap-dlts

./gocapfritz --extcap-config

./gocapfritz --capture -p <password> -h <host> --fifo <path_to_file> -i <interface>

./gocapfritz --capture -p topsecret123 -h fritz.box --fifo /tmp/fritzout.pcap -i 3-2
```

## Options

| Option | Description |
| ------ | ------ |
| `--extcap-interfaces` | Prints interfaces available to extcap  |
| `--extcap-interface` | Set extcap interface |
| `--extcap-dlts` | Prints extcap DLTs |
| `--extcap-config` | Prints extcap config |
| `--extcap-version` | Set extcap version |
| `--extcap-reload-option` | Reload option for provided string |
| `--capture` | Set extcap capture active |
| `--fifo` | Path to output pcap file |
| `--help` | Show help |
| `-p` | FRITZ!Box password |
| `-h` | FRITZ!Box host (URL or IP) |
| `-i` | FRITZ!Box capture interface |
| `-l` | FRITZ!Box capture packet length in bytes (default 1600) |

## Integrate with Wireshark

### Find the extcap folder

Open Wireshark and go to `Help->About Wireshark->Folders`.

The folder is located under `Global Extcap path`.

### Copy plugin to extcap Folders

Copy `gocapfritz` to the extcap folder. Restarting Wireshark should now already show various Fritz!Box interfaces.

### Usage

After opening Wireshark the Fritz!Box capture interface in the capture list should now be visible. Click the gear wheel button to open the capture interface options.

![wireshark_interfaces](resources/wireshark_interfaces.png "Wireshark interfaces")

Clicking the "Load interfaces" button after filling in host and password will query the Fritz!Box for available interfaces and show them in the dropdown list.

![extcap_options](resources/extcap_options.png "Extcap options")

Choose a interface from the dropdown list and click the start button.

![extcap_options_filled](resources/extcap_options_filled.png "Extcap options filled")

Now network traffic should appear in Wireshark.

## Bugs

### Password not cached

Restarting the Wireshark capture does not work, as the password is not cached.
