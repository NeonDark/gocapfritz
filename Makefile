BUILD_DIR := "build"

.PHONY: build linux windows darwin

all: build

build : linux windows darwin

darwin: ## Build darwin
	GOOS=darwin GOARCH=amd64 go build -o $(BUILD_DIR)/darwin_amd64_gocapfritz .

windows: ## Build windows
	GOOS=windows GOARCH=amd64 go build -o $(BUILD_DIR)/windows_amd64_gocapfritz.exe .

linux: ## Build linux
	GOOS=linux GOARCH=amd64 go build -o $(BUILD_DIR)/linux_amd64_gocapfritz .


help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
